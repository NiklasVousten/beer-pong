import core.Match;
import core.PartyLoewenScheduler;
import core.Scheduler;
import core.Team;
import gui.Window;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DebugMain {

    public static void main(String[] args){
        //pairingTest();
        //runTest();
        guiTest();
    }

    private static void guiTest() {
        Window window = new Window("GUI");
    }

    private static void runTest() {
        Team team1 = new Team("test1");
        Team team2 = new Team("test2");
        Team team3 = new Team("test3");
        Team team4 = new Team("test4");
        Team team5 = new Team("test5");
        Team team6 = new Team("test6");
        Team team7 = new Team("test7");
        Team team8 = new Team("test8");
        Team team9 = new Team("test9");
        Team team10 = new Team("test10");
        Team team11 = new Team("test11");
        Team team12 = new Team("test12");
        Team team13 = new Team("test13");

        PartyLoewenScheduler scheduler = new PartyLoewenScheduler();

        System.out.println("-----------------");
        System.out.println("Add Team1: " + scheduler.addTeam(team1));
        System.out.println("Add Team2: " + scheduler.addTeam(team2));
        System.out.println("Add Team3: " + scheduler.addTeam(team3));
        System.out.println("Add Team4: " + scheduler.addTeam(team4));
        System.out.println("Add Team5: " + scheduler.addTeam(team5));
        System.out.println("Add Team6: " + scheduler.addTeam(team6));
        System.out.println("Add Team7: " + scheduler.addTeam(team7));
        //System.out.println("Add Team8: " + scheduler.addTeam(team8));
        //System.out.println("Add Team9: " + scheduler.addTeam(team9));
        //System.out.println("Add Team10: " + scheduler.addTeam(team10));
        //System.out.println("Add Team11: " + scheduler.addTeam(team11));
        //System.out.println("Add Team12: " + scheduler.addTeam(team12));
        //System.out.println("Add Team13: " + scheduler.addTeam(team13));

        System.out.println(scheduler.getAllValidCombinations().size());

        System.out.println("-----------------");
        System.out.println("Schedule Round1: " + scheduler.createNextGames());
        setResults(scheduler);

        //System.out.println("-----------------");
        //System.out.println("Add Team8: " + scheduler.addTeam(team8));

        System.out.println("-----------------");
        System.out.println("Schedule Round2: " + scheduler.createNextGames());
        setResults(scheduler);

        System.out.println("-----------------");
        System.out.println("Schedule Round3: " + scheduler.createNextGames());
        setResults(scheduler);

        System.out.println("-----------------");
        System.out.println("Schedule Round4: " + scheduler.createNextGames());
        setResults(scheduler);

        System.out.println("-----------------");
        System.out.println("Schedule Round5: " + scheduler.createNextGames());
        setResults(scheduler);

        System.out.println("-----------------");
        System.out.println("Schedule Round6: " + scheduler.createNextGames());
        setResults(scheduler);

        System.out.println("-----------------");
        System.out.println("Schedule Round7: " + scheduler.createNextGames());

        setResults(scheduler);

        System.out.println(scheduler.getAllValidCombinations().size());
    }

    private static void pairingTest() {
        Team team1 = new Team("test1");
        Team team2 = new Team("test2");
        Team team3 = new Team("test3");
        Team team4 = new Team("test4");

        PartyLoewenScheduler scheduler = new PartyLoewenScheduler();

        System.out.println("-----------------");
        System.out.println("Add Team1: " + scheduler.addTeam(team1));
        System.out.println("Add Team2: " + scheduler.addTeam(team2));
        System.out.println("Add Team3: " + scheduler.addTeam(team3));
        System.out.println("Add Team4: " + scheduler.addTeam(team4));
        System.out.println("-----------------");

        List<Team[]> pairings = scheduler.getAllValidCombinations();
        System.out.println(pairings.size());

        System.out.println("-----------------");

        scheduler.getAllPossibleMatches(pairings);
    }

    private static void setResults(Scheduler scheduler){
        Random random = new Random();
        Match[] matches = scheduler.getMatchArray();
        for(int i=0; i<matches.length; i++){
            matches[i].setResult(0, random.nextInt(5)+1);
        }
    }
}

package gui;

import core.PartyLoewenScheduler;
import core.Team;

import javax.swing.*;
import java.awt.*;

public class Window extends JFrame{

    public Window(String title) throws HeadlessException {
        super(title);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300,300);

        //JPanel matchesPanel = new JPanel();
        //matchesPanel.setLayout(new GridLayout(6,1));

        //MatchPanel match1 = new MatchPanel();
        //MatchPanel match2 = new MatchPanel();
        //matchesPanel.add(match1); // Adds Button to content pane of frame
        //matchesPanel.add(match2); // Adds Button to content pane of frame

        //getContentPane().add(matchesPanel);

        //Team t1 = new Team("Team1");
        //TeamPanel panel = new TeamPanel(t1);

        getContentPane().add(new TeamContainer(new PartyLoewenScheduler(), this));

        setVisible(true);
    }
}

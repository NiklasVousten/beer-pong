package gui;

import core.Team;

import javax.swing.*;

public class TeamPanel extends JPanel {

    private Team team;
    private JLabel winsLabel;
    private JLabel scoreLabel;

    public TeamPanel(Team team){
        this.team = team;

        JLabel teamLabel = new JLabel(team.getTeamname());

        winsLabel = new JLabel();
        scoreLabel = new JLabel();

        add(teamLabel);
        add(winsLabel);
        add(scoreLabel);

        updatePanel();
    }

    public void updatePanel(){
        winsLabel.setText(String.format("Wins: %s", team.getWins()));
        scoreLabel.setText(String.format("Points: %s", team.getPoints()));
    }

}

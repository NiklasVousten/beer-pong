package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

public class MatchPanel extends JPanel {

    private JFormattedTextField team1PointsField;
    private JFormattedTextField team2PointsField;

    private JButton setResultButton;

    public MatchPanel() {
        team1PointsField = new JFormattedTextField(NumberFormat.getNumberInstance());
        team1PointsField.setColumns(2);
        team1PointsField.setValue(0L);

        team2PointsField = new JFormattedTextField(NumberFormat.getNumberInstance());
        team2PointsField.setColumns(2);
        team2PointsField.setValue(0L);

        setResultButton = new JButton("SetResult");
        setResultButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onSetResult();
            }
        });

        add(new JLabel("Team1"));
        add(team1PointsField);
        add(new JLabel("-"));
        add(team2PointsField);
        add(new JLabel("Team2"));
        add(setResultButton);

        setLayout(new GridLayout(1,6));
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    private void onSetResult() {
        long team1Points = (long)team1PointsField.getValue();
        long team2Points = (long)team2PointsField.getValue();

        if (team1Points==team2Points)
            return;

        team1PointsField.setEnabled(false);
        team2PointsField.setEnabled(false);

        setResultButton.setEnabled(false);
    }
}

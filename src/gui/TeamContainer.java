package gui;

import core.Scheduler;
import core.Team;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TeamContainer extends JPanel {

    private Scheduler scheduler;
    private JFrame parent;

    private GridLayout layout;
    private AddTeamDialog dialog;

    public TeamContainer(Scheduler scheduler, JFrame parent) {
        super();
        this.scheduler = scheduler;
        this.parent = parent;

        dialog = new AddTeamDialog(parent);

        layout = new GridLayout(9,1);
        setLayout(layout);

        JButton addTeamButton = new JButton("Add new Team");
        addTeamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addTeamHandler();
            }
        });

        add(addTeamButton);
    }

    public void addTeamHandler(){
        System.out.println("TEAM HANDLER");
        int result = dialog.showConfirmDialog();
        if (result == AddTeamDialog.OK_OPTION) {
            int rows = layout.getRows();
            if((rows - 1) == scheduler.teamcount()) {
                layout.setRows(layout.getRows() + 1);
            }

            String teamname = dialog.getTeamname();
            Team newTeam = new Team(teamname);

            System.out.println(teamname);

            scheduler.addTeam(newTeam);
            add(new TeamPanel(newTeam));
        } else {
            System.out.println("CHANCELD");
        }
    }

}

class AddTeamDialog extends JDialog {

    // STOLE FROM
    // http://www.java2s.com/Tutorials/Java/Swing_How_to/JDialog/Extend_JDialog_to_create_confirmation_dialog.htm

    public static final int OK_OPTION = 0;
    public static final int CANCEL_OPTION = 1;

    private int result = -1;

    JPanel content;

    JTextField teamnameField;

    public AddTeamDialog(Frame owner) {
        super(owner, true);

        setTitle("Add new Team");

        JPanel gui = new JPanel(new BorderLayout(3, 3));
        gui.setBorder(new EmptyBorder(5, 5, 5, 5));

        content = new JPanel(new GridLayout(1,2));
        content.add(new JLabel("Teamname"));

        teamnameField = new JTextField(10);
        teamnameField.setText("");
        content.add(teamnameField);

        gui.add(content, BorderLayout.CENTER);

        JPanel buttons = new JPanel(new FlowLayout(4));
        gui.add(buttons, BorderLayout.SOUTH);

        JButton ok = new JButton("OK");
        buttons.add(ok);
        ok.addActionListener(e->{
            result = OK_OPTION;
            setVisible(false);
        });

        JButton cancel = new JButton("Cancel");
        buttons.add(cancel);
        cancel.addActionListener(e->{
            result = CANCEL_OPTION;
            setVisible(false);
        });
        setContentPane(gui);

        pack();
    }

    public int showConfirmDialog() {

        teamnameField.setText("");
        //content.add(child, BorderLayout.CENTER);

        setLocationRelativeTo(getParent());
        setVisible(true);
        return result;
    }

    public String getTeamname(){
        return teamnameField.getText();
    }
}

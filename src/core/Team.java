package core;


public class Team implements Comparable<Team> {

    private String teamname;

    private int wins;
    private int points;

    public Team(String teamname){
        this.teamname = teamname;

        wins = 0;
        points = 0;
    }

    public String getTeamname() {
        return teamname;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void incWins() {
        wins++;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void addPoints(int points) {
        this.points += points;
    }

    @Override
    public int compareTo(Team o) {
        int result = Integer.compare(wins, o.getWins());
        if (result != 0)
                return result;
        return Integer.compare(points, o.getPoints());
    }
}

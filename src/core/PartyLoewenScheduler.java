package core;

import java.sql.Array;
import java.util.*;

public class PartyLoewenScheduler extends Scheduler{

    private int preRounds;
    private int postRounds;

    private List<Team> finalTeams;

    private Random random;

    public PartyLoewenScheduler() {
        super();

        preRounds = 4;
        postRounds = 0;

        random = new Random();
    }

    @Override
    protected void createMatchPairing() {  //First Phase Monrad System
        if (preRounds > 0){
            preRounds();
        } else {
            postRounds();
        }
    }

    private void preRounds(){
        preRounds--;
        if (finishedGames.isEmpty()) {
            firstRound();
        } else {
            scoredRounds();
        }
    }

    private void firstRound(){
        if (teamList.size() % 2 != 0) {
            teamList.add(new EmptyTeam());
        }

        List<Team> copyOfTeams = new ArrayList<>(teamList);
        printTeams(copyOfTeams);

        int selectNext;
        while (!copyOfTeams.isEmpty()) {
            Match nextMatch;

            selectNext = random.nextInt(copyOfTeams.size());
            Team team1 = copyOfTeams.get(selectNext);
            copyOfTeams.remove(selectNext);

            selectNext = random.nextInt(copyOfTeams.size());
            Team team2 = copyOfTeams.get(selectNext);
            copyOfTeams.remove(selectNext);

            nextMatch = new Match(team1, team2);

            currentGames.add(nextMatch);
            System.out.println(team1.getTeamname() + " vs. " + team2.getTeamname());
        }

        System.out.println("Random Matching");
    }

    private void scoredRounds(){
        List<Team> copyOfTeams = new ArrayList<>(teamList);
        copyOfTeams.sort(new TeamPointComparator());
        printTeams(copyOfTeams);

        List<Team[]> pairings = getAllValidCombinations();
        List<List<Team[]>> possibleMatches = getAllPossibleMatches(pairings);

        sortPossibleMatches(possibleMatches);

        List<Team[]> selectedMatches = possibleMatches.get(0);
        for(Team[] match: selectedMatches) {
            currentGames.add(new Match(match[0], match[1]));
            System.out.println(match[0].getTeamname() + " vs. " + match[1].getTeamname());
        }

        System.out.println("Point Matching");
    }

    private void postRounds(){
        if (postRounds++ == 0){
            List<Team> copyOfTeams = new ArrayList<>(teamList);
            Collections.sort(copyOfTeams);
            printTeams(copyOfTeams);

            int teamsToProceed = 8;
            if(copyOfTeams.size() < teamsToProceed){
                teamsToProceed = copyOfTeams.size();
            }

            finalTeams = new ArrayList<>();
            for(int i=0; i<teamsToProceed; i++){
                Team t = copyOfTeams.get(copyOfTeams.size() - 1);
                copyOfTeams.remove(copyOfTeams.size() - 1);

                finalTeams.add(new Team(t.getTeamname()));
            }

            copyOfTeams = new ArrayList<>(finalTeams);



            System.out.println("Random Tree Matching");
        } else {
            System.out.println("Tree Matching");
        }
    }

    private void printTeams(List<Team> teams) {
        System.out.println("------ Scores ------");
        for (Team t: teams) {
            System.out.println(String.format("%s: %s - %s", t.getTeamname(), t.getWins(), t.getPoints()));
        }
        System.out.println("--------------------");
    }

    public List<Team[]> getAllValidCombinations() {
        List<Team> copyOfTeams = new ArrayList<>(teamList);
        List<Team[]> permutations = new ArrayList<>();

        while(copyOfTeams.size() > 1){
            Team nextTeam = copyOfTeams.get(0);
            copyOfTeams.remove(0);

            for (Team t: copyOfTeams) {
                permutations.add(new Team[]{nextTeam, t});
            }
        }

        for(Match m: finishedGames){
            Team t1 = m.getTeam1();
            Team t2 = m.getTeam2();

            for(int i=(permutations.size()-1); i>=0; i--){
                Team[] pairing = permutations.get(i);
                if(pairing[0] == t1 && pairing[1] == t2){
                    permutations.remove(i);
                } else if(pairing[0] == t2 && pairing[1] == t1){
                    permutations.remove(i);
                }
            }
        }

        return permutations;
    }

    public List<List<Team[]>> getAllPossibleMatches(List<Team[]> pairings){
        List<List<Team[]>> possibleMatches = possibleMatchesHelper(new ArrayList<>(), new ArrayList<>(pairings), teamList.size() / 2);

        for(int i=(possibleMatches.size()-1); i>=0; i--) {
            boolean[] hasMatch = new boolean[teamList.size()];
            for(Team[] match: possibleMatches.get(i)){
                boolean isPresent = false;
                int teamIndex1 = teamList.indexOf(match[0]);  // Team1
                if (!hasMatch[teamIndex1]) {
                    hasMatch[teamIndex1] = true;
                } else {
                    isPresent = true;
                }

                int teamIndex2 = teamList.indexOf(match[1]); // Team2
                if (!hasMatch[teamIndex2]) {
                    hasMatch[teamIndex2] = true;
                } else {
                    isPresent = true;
                }

                if(isPresent) {
                    possibleMatches.remove(i);
                    break;
                }
            }
        }

        return possibleMatches;
    }

    public List<List<Team[]>> possibleMatchesHelper(List<Team[]> combinations, List<Team[]> data, int size) {
        size--;
        if(size == 0){
            List<List<Team[]>> result = new ArrayList<>();
            for(int i=0; i<data.size(); i++){
                List<Team[]> matchPairing = new ArrayList<>(combinations);
                matchPairing.add(data.get(i));
                result.add(matchPairing);
            }
            return result;
        } else {
            List<List<Team[]>> result = new ArrayList<>();
            for(int i=0; i<data.size(); i++){
                List<Team[]> matchPairing = new ArrayList<>(combinations);
                List<Team[]> dataCopy = new ArrayList<>(data);

                Team[] match = data.get(i);
                matchPairing.add(match);
                dataCopy.remove(match);

                List<List<Team[]>> results = possibleMatchesHelper(matchPairing, dataCopy, size);
                for(List<Team[]> matches: results){
                    result.add(matches);
                }
            }

            return result;
        }
    }

    private void sortPossibleMatches(List<List<Team[]>> matches){
        Collections.sort(matches, new Comparator<List<Team[]>>() {
            @Override
            public int compare(List<Team[]> o1, List<Team[]> o2) {
                int scoreObj1 = 0;
                for(Team[] match: o1) {
                    int scoreDiff = Math.abs(match[0].getPoints() - match[1].getPoints());
                    scoreObj1 += scoreDiff * scoreDiff; //Sqared to ensure a more balanced matching
                }

                int scoreObj2 = 0;
                for(Team[] match: o2) {
                    int scoreDiff = Math.abs(match[0].getPoints() - match[1].getPoints());
                    scoreObj2 += scoreDiff * scoreDiff; //Sqared to ensure a more balanced matching
                }

                return Integer.compare(scoreObj1, scoreObj2);
            }
        });

        //Collections.reverse(matches);
    }

    public void printPossibleMatches(List<List<Team[]>> matches) {
        for(List<Team[]> round: matches){
            System.out.println("-----------------");
            for(Team[] match: round) {
                System.out.println(String.format("[ %s - %s ]", match[0].getTeamname(), match[1].getTeamname()));
            }
        }
    }
}

package core;

public class Match {

    private Team team1;
    private Team team2;

    private int score1;
    private int score2;

    private boolean finished;

    public Match(Team team1, Team team2){
        this.team1 = team1;
        this.team2 = team2;

        finished = false;

        score1 = 0;
        score2 = 0;

        if (team1 instanceof EmptyTeam) {
            finished = true;
            team2.incWins();
        } else if (team2 instanceof EmptyTeam) {
            finished = true;
            team1.incWins();
        }
    }

    public void setResult(int cupsLeftTeam1, int cupsLeftTeam2) {
        if (finished)
            return;

        finished = true;
        score1 = cupsLeftTeam1;
        score2 = cupsLeftTeam2;

        int cupDifference = Math.abs(cupsLeftTeam1 - cupsLeftTeam2);
        if (cupsLeftTeam1 > cupsLeftTeam2) {
            team1.incWins();
            team1.addPoints(cupDifference);
            //Maybe disable
            team2.addPoints(-cupDifference);
        } else if (cupsLeftTeam1 < cupsLeftTeam2) {
            team2.incWins();
            team2.addPoints(cupDifference);
            //Maybe disable
            team1.addPoints(-cupDifference);
        } else { //Tie because of free game
            System.out.println("ERROR");
        }
    }

    public boolean isFinished() {
        return finished;
    }

    public Team getTeam1(){
        return team1;
    }

    public Team getTeam2(){
        return team2;
    }
}

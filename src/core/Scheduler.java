package core;

import java.util.ArrayList;
import java.util.List;

public abstract class Scheduler {

    protected List<Match> currentGames;
    protected List<Match> finishedGames;

    protected List<Team> teamList;

    public Scheduler() {
        currentGames = new ArrayList<>();
        finishedGames = new ArrayList<>();
        teamList = new ArrayList<>();
    }

    /**
     * Adds a team to the tournament. The operation will only be successfull, if the tournament is not running
     * @param team
     * @return True if operation was successfull, false if tournament is running
     */
    public boolean addTeam(Team team) {
        if (currentGames.isEmpty() && finishedGames.isEmpty()) {
            teamList.add(team);
            return true;
        }
        return false;
    }

    /**
     * Will create the next games, if all current Games are played.
     * @return True if operation was successfull, false if there are matches to be played
     */
    public boolean createNextGames() {
        checkForFinishedGames();

        if (currentGames.isEmpty()) {
            createMatchPairing();
            return true;
        }
        return false;
    }

    /**
     * Will move all finished Games from current to finished List
     */
    private void checkForFinishedGames() {
        int index = 0;
        while(index < currentGames.size()){
            if (currentGames.get(index).isFinished()){
                finishedGames.add(currentGames.get(index));
                currentGames.remove(index);
            } else {
                index++;
            }
        }
    }

    public int teamcount(){
        return teamList.size();
    }

    public Match[] getMatchArray() {
        Match[] matches = new Match[currentGames.size()];
        for (int i=0; i < currentGames.size(); i++){
            matches[i] = currentGames.get(i);
        }
        return matches;
    }

    protected abstract void createMatchPairing();

}
